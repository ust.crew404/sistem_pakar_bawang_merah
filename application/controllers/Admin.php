<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {


	function __construct(){
		parent::__construct();
        $this->load->database();
		$this->load->helper('text');
		$this->load->model('crud');
		$this->load->helper('url');
		if($this->session->userdata('status') != "login"){
			redirect(base_url("/login"));
		}
	}

	public function index()
	{
    redirect(base_url().'admin/dashboard');
	}

  public function dashboard()
  {
    $where = array('level' => 'user');
    $data['total_user'] = $this->crud->Get_User('tb_user', $where)->num_rows();
    $data['total_gejala'] = $this->crud->tampil_data('tb_gejala')->num_rows();
    $data['total_penyakit'] = $this->crud->tampil_data('tb_penyakit')->num_rows();
    $this->load->view('back/plugin/header');
    $this->load->view('back/index', $data);
    $this->load->view('back/plugin/footer');
  }
  public function daftar_gejala()
  {
    $data['daftar_gejala'] = $this->crud->tampil_data('tb_gejala')->result();
    $this->load->view('back/plugin/header', $data);
    $this->load->view('back/listUrl', $data);
    $this->load->view('back/plugin/footer', $data);
  }
  public function daftar_penyakit()
  {
    $data['daftar_penyakit'] = $this->crud->tampil_data('tb_penyakit')->result();
    $this->load->view('back/plugin/header', $data);
    $this->load->view('back/daftar_penyakit', $data);
    $this->load->view('back/plugin/footer', $data);
  }
  public function daftar_solusi()
  {
    $data['daftar_solusi'] = $this->crud->tampil_data('tb_solusi')->result();
    $this->load->view('back/plugin/header', $data);
    $this->load->view('back/daftar_solusi', $data);
    $this->load->view('back/plugin/footer', $data);
  }
  public function add_gejala()
  {
      $kode_gejala = $this->input->post('kg');
      $nama_gejala = $this->input->post('ng');
      $data = array(
                  'kode_gejala' => $kode_gejala,
                  'nama_gejala' => $nama_gejala
            );
      $this->crud->Insert('tb_gejala', $data);
      $this->session->set_flashdata('sukses', 'Tambah Gejala sukses.');
      redirect(base_url().'admin/daftar_gejala');
  }
  public function add_penyakit()
  {
      $kode_penyakit = $this->input->post('kg');
      $nama_penyakit = $this->input->post('ng');
      $data = array(
                  'kode_penyakit' => $kode_penyakit,
                  'nama_penyakit' => $nama_penyakit
            );
      $this->crud->Insert('tb_penyakit', $data);
      $this->session->set_flashdata('sukses', 'Tambah Penyakit sukses.');
      redirect(base_url().'admin/daftar_penyakit');
  }
  public function add_solusi()
  {
      $kode_solusi = $this->input->post('kg');
      $nama_solusi = $this->input->post('ng');
      $data = array(
                  'kode_solusi' => $kode_solusi,
                  'nama_solusi' => $nama_solusi
            );
      $this->crud->Insert('tb_solusi', $data);
      $this->session->set_flashdata('sukses', 'Tambah Solusi sukses.');
      redirect(base_url().'admin/daftar_solusi');
  }

  public function edit_gejala($id)
  {
    if(!empty($id)){
      $where = array('id' => $id);
      $data['get_where'] = $this->crud->Get_where('tb_gejala', $where);
      $this->load->view('back/plugin/header', $data);
      $this->load->view('back/edit_url', $data);
      $this->load->view('back/plugin/footer', $data);
    }else{
      redirect(base_url().'admin/daftar_gejala');
    }
  }
  public function edit_penyakit($id)
  {
    if(!empty($id)){
      $where = array('id' => $id);
      $data['get_where'] = $this->crud->Get_where('tb_penyakit', $where);
      $this->load->view('back/plugin/header', $data);
      $this->load->view('back/edit_penyakit', $data);
      $this->load->view('back/plugin/footer', $data);
    }else{
      redirect(base_url().'admin/daftar_gejala');
    }
  }
  public function edit_solusi($id)
  {
    if(!empty($id)){
      $where = array('id' => $id);
      $data['get_where'] = $this->crud->Get_where('tb_solusi', $where);
      $this->load->view('back/plugin/header', $data);
      $this->load->view('back/edit_solusi', $data);
      $this->load->view('back/plugin/footer', $data);
    }else{
      redirect(base_url().'admin/daftar_gejala');
    }
  }
  public function edit_act(){
    $id = $this->input->post('id');
    $kode_gejala = $this->input->post('kg');
    $nama_gejala = $this->input->post('ng');

    $where = array('id' => $id);
    $data = array(
                  'kode_gejala' => $kode_gejala,
                  'nama_gejala' => $nama_gejala
                );
    $this->crud->update_data($where,$data,'tb_gejala');
    $this->session->set_flashdata('sukses', 'Edit Gejala sukses.');

    redirect(base_url().'admin/daftar_gejala');
  }
  public function edit_penyakit_act(){
    $id = $this->input->post('id');
    $kode_penyakit = $this->input->post('kg');
    $nama_penyakit = $this->input->post('ng');

    $where = array('id' => $id);
    $data = array(
                  'kode_penyakit' => $kode_penyakit,
                  'nama_penyakit' => $nama_penyakit
                );
    $this->crud->update_data($where,$data,'tb_penyakit');
    $this->session->set_flashdata('sukses', 'Edit Penyakit sukses.');

    redirect(base_url().'admin/daftar_penyakit');
  }
  public function edit_solusi_act(){
    $id = $this->input->post('id');
    $kode_solusi = $this->input->post('kg');
    $nama_solusi = $this->input->post('ng');

    $where = array('id' => $id);
    $data = array(
                  'kode_solusi' => $kode_solusi,
                  'nama_solusi' => $nama_solusi
                );
    $this->crud->update_data($where,$data,'tb_solusi');
    $this->session->set_flashdata('sukses', 'Edit Solusi sukses.');

    redirect(base_url().'admin/daftar_solusi');
  }
  public function delete_gejala($id)
  {
    if(!empty($id)){
      $where = array('id' => $id);
      $this->crud->hapus_data($where,'tb_gejala');
      $this->session->set_flashdata('sukses', 'Gejala Berhasil di hapus.');
      redirect(base_url().'admin/daftar_gejala');
    }else{
      redirect(base_url().'admin/daftar_gejala');
    }
  }
  public function delete_penyakit($id)
  {
    if(!empty($id)){
      $where = array('id' => $id);
      $this->crud->hapus_data($where,'tb_penyakit');
      $this->session->set_flashdata('sukses', 'Penyakit Berhasil di hapus.');
      redirect(base_url().'admin/daftar_penyakit');
    }else{
      redirect(base_url().'admin/daftar_penyakit');
    }
  }
  public function delete_solusi($id)
  {
    if(!empty($id)){
      $where = array('id' => $id);
      $this->crud->hapus_data($where,'tb_solusi');
      $this->session->set_flashdata('sukses', 'Solusi Berhasil di hapus.');
      redirect(base_url().'admin/daftar_solusi');
    }else{
      redirect(base_url().'admin/daftar_solusi');
    }
  }
  public function riwayat_user(){
    // $total_klik = $this->crud->hitung('statistic', )
    $data['riwayat_user'] = $this->crud->tampil_data('konsultasi')->result();
    $this->load->view('back/plugin/header', $data);
    $this->load->view('back/statistic', $data);
    $this->load->view('back/plugin/footer', $data);
  }
  function logout(){
    $this->session->sess_destroy();
    redirect(base_url());
  }
}