<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {


	function __construct(){
		parent::__construct();
        $this->load->database();
		$this->load->helper('text');
		$this->load->model('crud');
		$this->load->helper('url');
		if($this->session->userdata('status') != "login"){
			redirect(base_url("/login"));
		}
	}
	public function index()
	{
    redirect(base_url().'user/dashboard');
	}
  
  public function dashboard()
  {
    $this->load->view('back_user/plugin/header');
    $this->load->view('back_user/index');
    $this->load->view('back_user/plugin/footer');
  }
  public function konsultasi()
  {
    $data['jenis_konsultasi'] = $this->crud->tampil_data('tb_gejala')->result();
    $this->load->view('back_user/plugin/header', $data);
    $this->load->view('back_user/konsultasi', $data);
    $this->load->view('back_user/plugin/footer', $data);
  }
  public function konsultasi_aksi()
  {
    error_reporting(0);
    $nA = 0;
    $nZ = 0;
    $cek = $this->crud->num('tb_gejala');
    for ($i=1; $i < $cek; $i++) {
      # code...
      $gejala[$i] = $this->input->post('nama'.$i);
      $teks[$i] = $this->input->post('gejala'.$i);
      if (is_null($gejala[$i])) {
        # code...
        unset($gejala[$i]);
        unset($teks[$i]);
      }
      $gejalaIn = array_values($gejala);
      $teks_gejala = array_values($teks);

    }

    $cek = count($gejalaIn);
    if ($cek == 2) {
      # code...\
      for ($a=0; $a < $cek; $a++) { 
        # code...
        // ringan
        if($gejalaIn[$a] <= 20){
           $miu_sedikit[$a] = 1;
           $z1[$a] = 1;
        }else if(($gejalaIn[$a] > 20) && ($gejalaIn[$a] < 30)){
             $miu_sedikit[$a] = (30 - $gejalaIn[$a]) / (30 - 20);
             $z1[$a] = 30 - ((30 - 20) * $miu_sedikit[$a]);
        }else if($gejalaIn[$a] >= 30){
             $miu_sedikit[$a] = 0;
             $z1[$a] = 0;
        }

        // SEDANG
        if($gejalaIn[$a] <= 20){
             $miu_sedang[$a] = 0;
             $z2[$a] = 0;
        }else if(($gejalaIn[$a] > 20) && ($gejalaIn[$a] < 30)){
             $miu_sedang[$a] = ($gejalaIn[$a] - 20) / (30 - 20);
             $z2[$a] = ((30 - 20) * $miu_sedang[$a]) + 20;
        }else if(($gejalaIn[$a] > 45) && ($gejalaIn[$a] < 55)){
             $miu_sedang[$a] = (55 - $gejalaIn[$a]) / (55 - 45);
             $z2[$a] = 55 - ((55 - 45) * $miu_sedang[$a]);
        }else if(($gejalaIn[$a] >= 30) && ($gejalaIn[$a] <= 45)){
             $miu_sedang[$a] = 1;
             $z2[$a] = 1;
        }
        // Banyak
        if($gejalaIn[$a] <= 45){
          $miu_banyak[$a] = 0;
          $z3[$a] = 0;
        }else if(($gejalaIn[$a] > 45) && ($gejalaIn[$a] < 55)){
          $miu_banyak[$a] = ($gejalaIn[$a] - 45) / (55 - 45);
          $z3[$a] = ((55 - 45) * $miu_banyak[$a]) + 20;
        }else if(($gejalaIn[$a] > 70) && ($gejalaIn[$a] < 80)){
          $miu_banyak[$a] = (80 - $gejalaIn[$a]) / (80 - 70);
          $z3[$a] = 80 - ((80 - 70) * $miu_banyak[$a]);
        }else if(($gejalaIn[$a] >= 55) && ($gejalaIn[$a] >= 70)){
          $miu_banyak[$a] = 1;
          $z3[$a] = 1;
        }
        # Sangat Banyak
        if($gejalaIn[$a] <= 70){
            $miu_sangat[$a] = 0;
            $z4[$a] = 0;
        }else if(($gejalaIn[$a] > 70) && ($gejalaIn[$a] < 80)){
            $miu_sangat[$a] = ($gejalaIn[$a]) - 70 / (80 - 70);
            $z4[$a] = 80 - ((80 - 70) * $miu_sangat[$a]);
        }else if($gejalaIn[$a] >= 80){
            $miu_sangat[$a] = 1;
            $z4[$a] = 1;
        }
      }
      for ($n=0; $n < $cek ; $n++) { 
        // a1
        if ($miu_sedikit[$n] < $miu_sedikit[$n+1]) {
                # code...
                $a1 = $miu_sedikit[$n];
                $Z1 = $z1[$n] * $a1;
            }elseif ($miu_sedikit[$n] > $miu_sedikit[$n+1]) {
                # code...
                $a1 = $miu_sedikit[$n+1];
                $Z1 = $z1[$n+1] * $a1;
            }
        // a2
        if ($miu_sedikit[$n] < $miu_sedang[$n+1]) {
                # code...
                $a2 = $miu_sedikit[$n];
                $Z2 = $z1[$n] * $a2;
        }elseif ($miu_sedikit[$n] > $miu_sedang[$n+1]) {
                # code...
                $a2 = $miu_sedang[$n+1];
                $Z2 = $z2[$n+1] * $a2;
        }
        // a3
        if ($miu_sedang[$n] < $miu_sedang[$n+1]) {
                # code...
                $a3 = $miu_sedang[$n];
                $Z3 = (30 - ((30 - 20) * $miu_sedang[$n])) * $a3;
        }elseif ($miu_sedang[$n] > $miu_sedang[$n+1]) {
                # code...
                $a3 = $miu_sedang[$n+1];
                $Z3 = $z2[$n+1] * $a3;
        }
        // a4
        if ($miu_sedang[$n] < $miu_sedikit[$n+1]) {
                # code...
                $a4 = $miu_sedang[$n];
                $Z4 = $z2[$n] * $a4;
        }elseif ($miu_sedang[$n] > $miu_sedikit[$n+1]) {
                # code...
                $a4 = $miu_sedikit[$n+1];
                $Z4 = $z1[$n+1] * $a4;
        }
        // a5
        if ($miu_banyak[$n] < $miu_banyak[$n+1]) {
                # code...
                $a5 = $miu_banyak[$n];
                $Z5 = $z3[$n] * $a5;
            }elseif ($miu_banyak[$n] > $miu_banyak[$n+1]) {
                # code...
                $a5 = $miu_banyak[$n+1];
                $Z5 = $z3[$n+1] * $a5;
            }
        // a6
        if ($miu_sedikit[$n] < $miu_banyak[$n+1]) {
                # code...
                $a6 = $miu_sedikit[$n];
                $Z6 = $z1[$n] * $a6;
        }elseif ($miu_sedikit[$n] > $miu_banyak[$n+1]) {
                # code...
                $a6 = $miu_banyak[$n+1];
                $Z6 = $z3[$n+1] * $a6;
        }
        // a7
        if ($miu_banyak[$n] < $miu_sedikit[$n+1]) {
                # code...
                $a7 = $miu_banyak[$n];
                $Z7 = $z3[$n] * $a7;
            }elseif ($miu_banyak[$n] > $miu_sedikit[$n+1]) {
                # code...
                $a7 = $miu_sedikit[$n+1];
                $Z7 = $z1[$n+1] * $a7;
            }
        // a8
        if ($miu_sedang[$n] < $miu_banyak[$n+1]) {
                # code...
                $a8 = $miu_sedang[$n];
                $Z8 = $z2[$n] * $a8;
        }elseif ($miu_sedang[$n] > $miu_banyak[$n+1]) {
                # code...
                $a8 = $miu_banyak[$n+1];
                $Z8 = $z3[$n+1] * $a8;
        }
        // a9
        if ($miu_banyak[$n] < $miu_sedang[$n+1]) {
                # code...
                $a9 = $miu_banyak[$n];
                $Z9 = $z3[$n] * $a9;
            }elseif ($miu_banyak[$n] > $miu_sedang[$n+1]) {
                # code...
                $a9 = $miu_sedang[$n+1];
                $Z9 = $z2[$n+1] * $a9;
            }
        // a10
        if ($miu_sangat[$n] < $miu_sangat[$n+1]) {
                # code...
                $a10 = $miu_sangat[$n];
                $Z10 = $z4[$n] * $a10;
            }elseif ($miu_sangat[$n] > $miu_sangat[$n+1]) {
                # code...
                $a10 = $miu_sangat[$n+1];
                $Z10 = $z3[$n+1] * $a10;
            }
          // a11
        if ($miu_sedikit[$n] < $miu_sangat[$n+1]) {
                # code...
                $a11 = $miu_sedikit[$n];
                $Z11 = $z1[$n] * $a11;
        }elseif ($miu_sedikit[$n] > $miu_sangat[$n+1]) {
                # code...
                $a11 = $miu_sangat[$n+1];
                $Z11 = $z4[$n+1] * $a11;
        }
        // a12
        if ($miu_sangat[$n] < $miu_sedikit[$n+1]) {
                # code...
                $a12 = $miu_sangat[$n];
                $Z12 = $z4[$n] * $a12;
            }elseif ($miu_sangat[$n] > $miu_sedikit[$n+1]) {
                # code...
                $a12 = $miu_sedikit[$n+1];
                $Z12 = $z1[$n+1] * $a12;
            }
        // a13
        if ($miu_sedang[$n] < $miu_sangat[$n+1]) {
                # code...
                $a13 = $miu_sedang[$n];
                $Z13 = $z2[$n] * $a13;
        }elseif ($miu_sedang[$n] > $miu_sangat[$n+1]) {
                # code...
                $a13 = $miu_sangat[$n+1];
                $Z13 = $z4[$n+1] * $a13;
        }
        // a14
        if ($miu_sangat[$n] < $miu_sedang[$n+1]) {
                # code...
                $a14 = $miu_sangat[$n];
                $Z14 = $z4[$n] * $a14;
            }elseif ($miu_sangat[$n] > $miu_sedang[$n+1]) {
                # code...
                $a14 = $miu_sedang[$n+1];
                $Z14 = $z2[$n+1] * $a14;
            }
        // a16
        if ($miu_banyak[$n] < $miu_sangat[$n+1]) {
                # code...
                $a16 = $miu_banyak[$n];
                $Z16 = $z3[$n] * $a16;
        }elseif ($miu_banyak[$n] > $miu_sangat[$n+1]) {
                # code...
                $a16 = $miu_sangat[$n+1];
                $Z16 = $z4[$n+1] * $a16;
        }
        // a15
        if ($miu_sangat[$n] < $miu_banyak[$n+1]) {
                # code...
                $a15 = $miu_sangat[$n];
                $Z15 = $z4[$n] * $a15;
            }elseif ($miu_sangat[$n] > $miu_banyak[$n+1]) {
                # code...
                $a15 = $miu_sedang[$n+1];
                $Z15 = $z3[$n+1] * $a15;
            }

        // echo "nilai a = ".$a3." hasil z = ".$Z3." || ";
        $nA = $nA + $a1 + $a2 + $a3 + $a4 + $a5 + $a6 + $a7 + $a8 + $a9 + $a10 + $a11 + $a12 + $a13 + $a14 + $a15 + $a16;
        $nZ = $nZ + $Z1 + $Z2 + $Z3 + $Z4 + $Z5 + $Z6 + $Z7 + $Z8 + $Z9 + $Z10 + $Z11 + $Z12 + $Z13 + $Z14 + $Z15 + $Z16;
        $Z = $nZ / $nA;
      }
    }elseif ($cek == 3) {
      # code...
      for ($a=0; $a < $cek; $a++) { 
        # code...
        // ringan
        if($gejalaIn[$a] <= 20){
           $miu_sedikit[$a] = 1;
           $z1[$a] = 1;
        }else if(($gejalaIn[$a] > 20) && ($gejalaIn[$a] < 30)){
             $miu_sedikit[$a] = (30 - $gejalaIn[$a]) / (30 - 20);
             $z1[$a] = 30 - ((30 - 20) * $miu_sedikit[$a]);
        }else if($gejalaIn[$a] >= 30){
             $miu_sedikit[$a] = 0;
             $z1[$a] = 0;
        }

        // SEDANG
        if($gejalaIn[$a] <= 20){
             $miu_sedang[$a] = 0;
             $z2[$a] = 0;
        }else if(($gejalaIn[$a] > 20) && ($gejalaIn[$a] < 30)){
             $miu_sedang[$a] = ($gejalaIn[$a] - 20) / (30 - 20);
             $z2[$a] = ((30 - 20) * $miu_sedang[$a]) + 20;
        }else if(($gejalaIn[$a] > 45) && ($gejalaIn[$a] < 55)){
             $miu_sedang[$a] = (55 - $gejalaIn[$a]) / (55 - 45);
             $z2[$a] = 55 - ((55 - 45) * $miu_sedang[$a]);
        }else if(($gejalaIn[$a] >= 30) && ($gejalaIn[$a] <= 45)){
             $miu_sedang[$a] = 1;
             $z2[$a] = 1;
        }
        // Banyak
        if($gejalaIn[$a] <= 45){
          $miu_banyak[$a] = 0;
          $z3[$a] = 0;
        }else if(($gejalaIn[$a] > 45) && ($gejalaIn[$a] < 55)){
          $miu_banyak[$a] = ($gejalaIn[$a] - 45) / (55 - 45);
          $z3[$a] = ((55 - 45) * $miu_banyak[$a]) + 20;
        }else if(($gejalaIn[$a] > 70) && ($gejalaIn[$a] < 80)){
          $miu_banyak[$a] = (80 - $gejalaIn[$a]) / (80 - 70);
          $z3[$a] = 80 - ((80 - 70) * $miu_banyak[$a]);
        }else if(($gejalaIn[$a] >= 55) && ($gejalaIn[$a] >= 70)){
          $miu_banyak[$a] = 1;
          $z3[$a] = 1;
        }
        # Sangat Banyak
        if($gejalaIn[$a] <= 70){
            $miu_sangat[$a] = 0;
            $z4[$a] = 0;
        }else if(($gejalaIn[$a] > 70) && ($gejalaIn[$a] < 80)){
            $miu_sangat[$a] = ($gejalaIn[$a]) - 70 / (80 - 70);
            $z4[$a] = 80 - ((80 - 70) * $miu_sangat[$a]);
        }else if($gejalaIn[$a] >= 80){
            $miu_sangat[$a] = 1;
            $z4[$a] = 1;
        }
      }
      for ($n=0; $n < $cek ; $n++) {
        // a1
        if ($miu_sedikit[$n] < $miu_sedikit[$n+1]) {
                # code...
                $a1 = $miu_sedikit[$n];
                $Z1 = $z1[$n] * $a1;
            }elseif ($miu_sedikit[$n] > $miu_sedikit[$n+1]) {
                # code...
                $a1 = $miu_sedikit[$n+1];
                $Z1 = $z1[$n+1] * $a1;
            }

        // a2
        if ($miu_sedikit[$n] < $miu_sedikit[$n+2]) {
                # code...
                $a2 = $miu_sedikit[$n];
                $Z2 = $z1[$n] * $a2;
            }elseif ($miu_sedikit[$n] > $miu_sedikit[$n+2]) {
                # code...
                $a2 = $miu_sedikit[$n+2];
                $Z2 = $z1[$n+2] * $a2;
            }

        // a3
        if ($miu_sedikit[$n] < $miu_sedang[$n+1]) {
                # code...
                $a3 = $miu_sedikit[$n];
                $Z3 = $z1[$n] * $a3;
            }elseif ($miu_sedikit[$n] > $miu_sedang[$n+1]) {
                # code...
                $a3 = $miu_sedang[$n+1];
                $Z3 = $z2[$n+1] * $a3;
            }
          // a4
        if ($miu_sedikit[$n] < $miu_sedang[$n+2]) {
                # code...
                $a4 = $miu_sedikit[$n];
                $Z4 = $z1[$n] * $a4;
            }elseif ($miu_sedikit[$n] > $miu_sedang[$n+2]) {
                # code...
                $a4 = $miu_sedang[$n+2];
                $Z4 = $z2[$n+2] * $a4;
            }
          // a5
        if ($miu_sedang[$n] < $miu_sedikit[$n+1]) {
                # code...
                $a5 = $miu_sedang[$n];
                $Z5 = $z2[$n] * $a5;
            }elseif ($miu_sedang[$n] > $miu_sedikit[$n+1]) {
                # code...
                $a5 = $miu_sedikit[$n+1];
                $Z5 = $z1[$n+1] * $a5;
            }
        // a6
        if ($miu_sedang[$n] < $miu_sedikit[$n+2]) {
                # code...
                $a6 = $miu_sedang[$n];
                $Z6 = $z2[$n] * $a6;
            }elseif ($miu_sedang[$n] > $miu_sedikit[$n+2]) {
                # code...
                $a6 = $miu_sedikit[$n+2];
                $Z6 = $z1[$n+2] * $a6;
            }
        // a7
        if ($miu_sedikit[$n] < $miu_banyak[$n+1]) {
                # code...
                $a7 = $miu_sedikit[$n];
                $Z7 = $z1[$n] * $a7;
            }elseif ($miu_sedikit[$n] > $miu_banyak[$n+1]) {
                # code...
                $a7 = $miu_banyak[$n+1];
                $Z7 = $z3[$n+1] * $a7;
            }
        // a8
        if ($miu_sedikit[$n] < $miu_banyak[$n+2]) {
                # code...
                $a8 = $miu_sedikit[$n];
                $Z8 = $z1[$n] * $a8;
            }elseif ($miu_sedikit[$n] > $miu_banyak[$n+2]) {
                # code...
                $a8 = $miu_banyak[$n+2];
                $Z8 = $z3[$n+2] * $a8;
            }
          // a9
        if ($miu_banyak[$n] < $miu_sedikit[$n+1]) {
                # code...
                $a9 = $miu_banyak[$n];
                $Z9 = $z3[$n] * $a9;
            }elseif ($miu_banyak[$n] > $miu_sedikit[$n+1]) {
                # code...
                $a9 = $miu_sedikit[$n+1];
                $Z9 = $z1[$n+1] * $a9;
            }
        // a10
        if ($miu_banyak[$n] < $miu_sedikit[$n+2]) {
                # code...
                $a10 = $miu_banyak[$n];
                $Z10 = $z3[$n] * $a10;
            }elseif ($miu_banyak[$n] > $miu_sedikit[$n+2]) {
                # code...
                $a10 = $miu_sedikit[$n+2];
                $Z10 = $z1[$n+2] * $a10;
            }
        // a11
        if ($miu_sedang[$n] < $miu_banyak[$n+1]) {
                # code...
                $a11 = $miu_sedang[$n];
                $Z11 = $z2[$n] * $a11;
            }elseif ($miu_sedang[$n] > $miu_banyak[$n+1]) {
                # code...
                $a11 = $miu_banyak[$n+1];
                $Z11 = $z3[$n+1] * $a11;
            }
        // a12
        if ($miu_sedang[$n] < $miu_banyak[$n+2]) {
                # code...
                $a12 = $miu_sedang[$n];
                $Z12 = $z2[$n] * $a12;
            }elseif ($miu_sedang[$n] > $miu_banyak[$n+2]) {
                # code...
                $a12 = $miu_banyak[$n+2];
                $Z12 = $z3[$n+2] * $a12;
            }
          // a13
        if ($miu_banyak[$n] < $miu_sedang[$n+1]) {
                # code...
                $a13 = $miu_banyak[$n];
                $Z13 = $z3[$n] * $a13;
            }elseif ($miu_banyak[$n] > $miu_sedang[$n+1]) {
                # code...
                $a13 = $miu_sedang[$n+1];
                $Z13 = $z2[$n+1] * $a13;
            }
        // a14
        if ($miu_banyak[$n] < $miu_sedang[$n+2]) {
                # code...
                $a14 = $miu_banyak[$n];
                $Z14 = $z3[$n] * $a14;
            }elseif ($miu_banyak[$n] > $miu_sedang[$n+2]) {
                # code...
                $a14 = $miu_sedang[$n+2];
                $Z14 = $z2[$n+2] * $a14;
            }
        // a15
        if ($miu_sedikit[$n] < $miu_sangat[$n+1]) {
                # code...
                $a15 = $miu_sedikit[$n];
                $Z15 = $z1[$n] * $a15;
            }elseif ($miu_sedikit[$n] > $miu_sangat[$n+1]) {
                # code...
                $a15 = $miu_sangat[$n+1];
                $Z15 = $z4[$n+1] * $a15;
            }
        // a16
        if ($miu_sedikit[$n] < $miu_sangat[$n+2]) {
                # code...
                $a16 = $miu_sedikit[$n];
                $Z16 = $z1[$n] * $a16;
            }elseif ($miu_sedikit[$n] > $miu_sangat[$n+2]) {
                # code...
                $a16 = $miu_sangat[$n+2];
                $Z16 = $z4[$n+2] * $a16;
            }
          // a17
        if ($miu_sangat[$n] < $miu_sedikit[$n+1]) {
                # code...
                $a17 = $miu_sangat[$n];
                $Z17 = $z4[$n] * $a17;
            }elseif ($miu_sangat[$n] > $miu_sedikit[$n+1]) {
                # code...
                $a17 = $miu_sedikit[$n+1];
                $Z17 = $z1[$n+1] * $a17;
            }
        // a18
        if ($miu_sangat[$n] < $miu_sedikit[$n+2]) {
                # code...
                $a18 = $miu_sangat[$n];
                $Z18 = $z4[$n] * $a18;
            }elseif ($miu_sangat[$n] > $miu_sedikit[$n+2]) {
                # code...
                $a18 = $miu_sedikit[$n+2];
                $Z18 = $z1[$n+2] * $a18;
            }
        // a19
        if ($miu_sedang[$n] < $miu_sangat[$n+1]) {
                # code...
                $a19 = $miu_sedang[$n];
                $Z19 = $z2[$n] * $a19;
            }elseif ($miu_sedang[$n] > $miu_sangat[$n+1]) {
                # code...
                $a19 = $miu_sangat[$n+1];
                $Z19 = $z4[$n+1] * $a19;
            }
        // a20
        if ($miu_sedang[$n] < $miu_sangat[$n+2]) {
                # code...
                $a20 = $miu_sedang[$n];
                $Z20 = $z2[$n] * $a20;
            }elseif ($miu_sedang[$n] > $miu_sangat[$n+2]) {
                # code...
                $a20 = $miu_sangat[$n+2];
                $Z20 = $z4[$n+2] * $a20;
            }
          // a21
        if ($miu_sangat[$n] < $miu_sedang[$n+1]) {
                # code...
                $a21 = $miu_sangat[$n];
                $Z21 = $z4[$n] * $a21;
            }elseif ($miu_sangat[$n] > $miu_sedang[$n+1]) {
                # code...
                $a21 = $miu_sedang[$n+1];
                $Z21 = $z2[$n+1] * $a21;
            }
        // a22
        if ($miu_sangat[$n] < $miu_sedang[$n+2]) {
                # code...
                $a22 = $miu_sangat[$n];
                $Z22 = $z4[$n] * $a22;
            }elseif ($miu_sangat[$n] > $miu_sedang[$n+2]) {
                # code...
                $a22 = $miu_sedang[$n+2];
                $Z22 = $z2[$n+2] * $a22;
            }
        // a23
        if ($miu_banyak[$n] < $miu_sangat[$n+1]) {
                # code...
                $a23 = $miu_banyak[$n];
                $Z23 = $z3[$n] * $a23;
            }elseif ($miu_banyak[$n] > $miu_sangat[$n+1]) {
                # code...
                $a23 = $miu_sangat[$n+1];
                $Z23 = $z4[$n+1] * $a23;
            }
        // a24
        if ($miu_banyak[$n] < $miu_sangat[$n+2]) {
                # code...
                $a24 = $miu_banyak[$n];
                $Z24 = $z3[$n] * $a24;
            }elseif ($miu_banyak[$n] > $miu_sangat[$n+2]) {
                # code...
                $a24 = $miu_sangat[$n+2];
                $Z24 = $z4[$n+2] * $a24;
            }
          // a25
        if ($miu_sangat[$n] < $miu_banyak[$n+1]) {
                # code...
                $a25 = $miu_sangat[$n];
                $Z25 = $z4[$n] * $a25;
            }elseif ($miu_sangat[$n] > $miu_banyak[$n+1]) {
                # code...
                $a25 = $miu_banyak[$n+1];
                $Z25 = $z3[$n+1] * $a25;
            }
        // a26
        if ($miu_sangat[$n] < $miu_banyak[$n+2]) {
                # code...
                $a26 = $miu_sangat[$n];
                $Z26 = $z4[$n] * $a26;
            }elseif ($miu_sangat[$n] > $miu_banyak[$n+2]) {
                # code...
                $a26 = $miu_banyak[$n+2];
                $Z26 = $z3[$n+2] * $a26;
            }
        // a27
        if ($miu_sedang[$n] < $miu_sedang[$n+1]) {
                # code...
                $a27 = $miu_sedang[$n];
                $Z27 = $z2[$n] * $a27;
            }elseif ($miu_sedang[$n] > $miu_sedang[$n+1]) {
                # code...
                $a27 = $miu_sedang[$n+1];
                $Z27 = $z2[$n+1] * $a27;
            }
        // a28
        if ($miu_sedang[$n] < $miu_sedang[$n+2]) {
                # code...
                $a28 = $miu_sedang[$n];
                $Z28 = $z2[$n] * $a28;
            }elseif ($miu_sedang[$n] > $miu_sedang[$n+2]) {
                # code...
                $a28 = $miu_sedang[$n+2];
                $Z28 = $z2[$n+2] * $a28;
            }
        // a29
        if ($miu_banyak[$n] < $miu_banyak[$n+1]) {
                # code...
                $a29 = $miu_banyak[$n];
                $Z29 = $z3[$n] * $a29;
            }elseif ($miu_banyak[$n] > $miu_banyak[$n+1]) {
                # code...
                $a29 = $miu_banyak[$n+1];
                $Z29 = $z3[$n+1] * $a29;
            }
        // a30
        if ($miu_banyak[$n] < $miu_banyak[$n+2]) {
                # code...
                $a30 = $miu_banyak[$n];
                $Z30 = $z3[$n] * $a30;
            }elseif ($miu_banyak[$n] > $miu_banyak[$n+2]) {
                # code...
                $a30 = $miu_banyak[$n+2];
                $Z30 = $z3[$n+2] * $a30;
            }
        // a31
        if ($miu_sangat[$n] < $miu_sangat[$n+1]) {
                # code...
                $a31 = $miu_sangat[$n];
                $Z31 = $z4[$n] * $a31;
            }elseif ($miu_sangat[$n] > $miu_sangat[$n+1]) {
                # code...
                $a31 = $miu_sangat[$n+1];
                $Z31 = $z4[$n+1] * $a31;
            }
        // a32
        if ($miu_sangat[$n] < $miu_sangat[$n+2]) {
                # code...
                $a32 = $miu_sangat[$n];
                $Z32 = $z4[$n] * $a32;
            }elseif ($miu_sangat[$n] > $miu_sangat[$n+2]) {
                # code...
                $a32 = $miu_sangat[$n+2];
                $Z32 = $z4[$n+2] * $a32;
            }
        // echo "nilai a = ".$a3." hasil z = ".$Z3." || ";
        $nA = $nA + $a1 + $a2 + $a3 + $a4 + $a5 + $a6 + $a7 + $a8 + $a9 + $a10 + $a11 + $a12 + $a13 + $a14 + $a15 + $a16 + $a17 + $a18 + $a19 + $a20 + $a21 + $a22 + $a23 + $a24 + $a25 + $a26 + $a27 + $a28 + $a29 + $a30 + $a31 + $a32;
        $nZ = $nZ + $Z1 + $Z2 + $Z3 + $Z4 + $Z5 + $Z6 + $Z7 + $Z8 + $Z9 + $Z10 + $Z11 + $Z12 + $Z13 + $Z14 + $Z15 + $Z16 + $Z17 + $Z18 + $Z19 + $Z20 + $Z21 + $Z22 + $Z23 + $Z24 + $Z25 + $Z26 + $Z27 + $Z28 + $Z29 + $Z30 + $Z31 + $Z32;
        $Z = $nZ / $nA;
      }
    }

    $m_sedikit[] = $miu_sedikit;
    $m_sedang[] = $miu_sedang;
    // echo "gejala = ".$teks;\
    for ($i=0; $i < count($teks_gejala) ; $i++) { 
      # code...
      // $gejala_timbul[$i] = "Gejala penyakit ".$teks_gejala[$i]."dengan tingkat kerusakan ".$gejalaIn[$i]." dan Gejala penyakit ".$teks_gejala[$i+1]." dengan tingkat kerusakan ".$teks_gejala[$i+1];
      $gejala_timbul[] = $teks_gejala[$i];
      $where = array('nama_gejala' => $teks_gejala[$i]);
      $get_kode = $this->crud->Get_where('tb_gejala', $where);
      foreach ($get_kode as $kode) {
        # code...
        $kode_gejala = $kode->kode_gejala;
        $solusi = $this->db->query("SELECT tb_gejala.nama_gejala, tb_penyakit.nama_penyakit, tb_solusi.nama_solusi FROM tb_gejala INNER JOIN tb_penyakit ON tb_gejala.id = tb_penyakit.id INNER JOIN tb_solusi ON tb_penyakit.id = tb_solusi.id WHERE tb_gejala.kode_gejala = '$kode_gejala'")->result_array();
        // var_dump($solusi[0]['nama_gejala']);
      }
        $dbgejala[] = $solusi[0]["nama_gejala"];
        $dbpenyakit[] = $solusi[0]["nama_penyakit"];
        $dbsolusi[] = $solusi[0]["nama_solusi"];
        // $hasil_solusi[] = $solusi[0]["nama_gejala"]." , ".$solusi[0]["nama_penyakit"]." , ".$solusi[0]["nama_solusi"];
    }
    if (count($teks_gejala) == 2) {
      # code...
      for($i = 0; $i < count($teks_gejala); $i++){
        $savegejala[] = $dbgejala[$i]." , ".$dbgejala[$i+1];
        $savepenyakit[] = $dbpenyakit[$i]." , ".$dbpenyakit[$i+1];
        $savesolusi[] = $dbsolusi[$i]." , ".$dbsolusi[$i+1];
      }
    }elseif (count($teks_gejala) == 3) {
      # code...
      for($i = 0; $i < count($teks_gejala); $i++){
        $savegejala[] = $dbgejala[$i]." , ".$dbgejala[$i+1]." , ".$dbgejala[$i+2];
        $savepenyakit[] = $dbpenyakit[$i]." , ".$dbpenyakit[$i+1]." , ".$dbpenyakit[$i+2];
        $savesolusi[] = $dbsolusi[$i]." , ".$dbsolusi[$i+1]." , ".$dbsolusi[$i+2];
      }
    }
    if (($Z > 0) && ($Z <= 25)) {
      # code...
      $tingkat_serangan = "Tingkat Serangan Ringan";
    }elseif (($Z > 25) && ($Z <= 50)) {
      # code...
      $tingkat_serangan = "Tingkat Serangan Sedang";
    }elseif (($Z > 50) && ($Z <= 75)) {
      # code...
      $tingkat_serangan = "Tingkat Serangan Berat";
    }elseif ($Z > 75) {
      # code...
      $tingkat_serangan = "Tingkat Serangan Sangat Berat";
    }
    $data = array(
                  'nama' => $this->session->userdata('nama'),
                  'gejala' => $savegejala[0],
                  'penyakit' => $savepenyakit[0],
                  'solusi' => $savesolusi[0],
                  'hasilkonsultasi' => $Z,
                  'keterangan' => $tingkat_serangan
                    );
    $this->crud->Insert('konsultasi', $data);
    $data_session = array(
                    'hasil_perhitungan' => 'Dengan Jenis Gejala '.$savegejala[0].' dapat dipastikan jika hama yang menyerang adalah '.$savepenyakit[0].' maka solusi yang dapat diberikan yaitu : '.$savesolusi[0].' dan untuk tingkat serangan sebesar '.$Z.'% dapat dikategorikan '.$tingkat_serangan
                   );
    $this->session->set_userdata($data_session);
    redirect('/user/hasil_konsultasi');
    // echo "<br>";
    // echo "hasil defuzzyfikasi = ".$Z;
    // echo "<br>";
    // print_r($gejalaIn);
    // echo "<br>";
    // print_r($nZ);
    // echo "<br>";
    // print_r($nA);
    // echo "<br>";
  }
  public function hasil_konsultasi(){
    $this->load->view('back_user/plugin/header');
    $this->load->view('back_user/hasil_konsultasi');
    $this->load->view('back_user/plugin/footer');
  }
  public function riwayat_konsultasi(){
    $where = array('nama' => $this->session->userdata('nama'));
    $data['riwayat_konsultasi'] = $this->crud->Get_user('konsultasi', $where)->result();
    $this->load->view('back_user/plugin/header');
    $this->load->view('back_user/riwayat_konsultasi', $data);
    $this->load->view('back_user/plugin/footer');
  }
  function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}
}