<!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>SISTEM PAKAR BAWANG MERAH</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
        <meta name="keywords" content="bootstrap 4, premium, marketing, multipurpose" />
        <meta content="Shreethemes" name="author" />
        <!-- favicon -->
        <link rel="shortcut icon" href="<?php echo base_url().'assets/frontend/images/icon.ico'; ?>">
        
        <!-- datatable -->
        <script type="text/javascript" src="<?php echo base_url().'assets/frontend/js/jquery-3.3.1.js'; ?>"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <!-- datatable -->

        <!-- Bootstrap -->
        <link href="<?php echo base_url().'assets/frontend/css/bootstrap.min.css'; ?>" rel="stylesheet" type="text/css" />
        <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />


        <!-- Icons -->
        <link rel="stylesheet" href="//cdn.materialdesignicons.com/4.4.95/css/materialdesignicons.min.css">
        <!-- Slider -->               
        <link rel="stylesheet" href="<?php echo base_url().'assets/frontend/css/owl.carousel.min.css'; ?>"/> 
        <link rel="stylesheet" href="<?php echo base_url().'assets/frontend/css/owl.theme.css'; ?>"/> 
        <link rel="stylesheet" href="<?php echo base_url().'assets/frontend/css/owl.transitions.css'; ?>"/>  
        <!-- Main css --> 
        <link href="<?php echo base_url().'assets/frontend/css/style.css'; ?>" rel="stylesheet" type="text/css" />

    </head>

    <body>
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
        <!-- Loader -->