    <div class="page">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="javascript:void(0);">Konsultasi</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-align-justify"></i>
            </button>
        </nav>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card widget_2 big_icon traffic">
                        <div class="body">
                          <form action="<?php echo base_url();?>/user/konsultasi_aksi" method="post">
                            <h5>Konsultasi</h5>
                            <span>*Silahkan Pilih Gejala Yang Sesuai Dan Masukan Tingkat Serangan Tiap Gejala</span>
                            <?php 
                                $n = 1;
                                $n1 = 1; $n4 = 1; $n7 = 1;
                                $n2 = 1; $n5 = 1; $n8 = 1;
                                $n3 = 1; $n6 = 1; $n9 = 1; 
                                $n10= 1;
                                foreach ($jenis_konsultasi as $jk){
                                    echo "<hr><p>".$jk->kode_gejala .$jk->nama_gejala."?</p>";
                                    echo "<input type='radio' name='pilihan".$n9++."' onclick='myFunction".$n1++."()'> Tidak
                                        <input type='radio' name='pilihan".$n10++."' onclick='myOpen".$n2++."()'> Ya<br>";
                                    echo "<input type='number' class='form-control pl-5' id='myText".$n3++."' name='nama".$n4++."' disabled>";
                            ?>
                                <input name="gejala<?php echo $n++;?>" value="<?php echo $jk->nama_gejala; ?>" hidden>
                            <script>
                                function myFunction<?php echo $n5++; ?>() {
                                  document.getElementById("myText<?php echo $n6++;?>").disabled = true;
                                }
                                function myOpen<?php echo $n7++; ?>() {
                                  document.getElementById("myText<?php echo $n8++;?>").disabled = false;
                                }
                            </script>
                            <?php        
                                }
                            ?>
                            <hr>
                            <button class="btn btn-primary w-100">Proses</button>
                          </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>

