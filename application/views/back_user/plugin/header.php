<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="Webpixels">
<link rel="icon" href="<?php echo base_url().'assets/frontend/images/icon.ico'; ?>" type="image/x-icon">
<title>Dashboard</title>

<link rel="stylesheet" href="<?php echo base_url().'assets/backend/vendor/themify-icons/themify-icons.css'; ?>">
<!-- <link rel="stylesheet" href="<?php echo base_url().'assets/backend/vendor/fontawesome/css/font-awesome.min.css'; ?>"> -->
<link rel="stylesheet" href="<?php echo base_url().'assets/backend/vendor/charts-c3/plugin.css'; ?>"/>
<link rel="stylesheet" href="<?php echo base_url().'assets/backend/vendor/jvectormap/jquery-jvectormap-2.0.3.css'; ?>"/>
<link rel="stylesheet" href="<?php echo base_url().'assets/backend/css/main.css'; ?>" type="text/css">
</head>
<body class="theme-indigo">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img src="<?php echo base_url().'assets/frontend/images/illustrator/Startup_SVG.png'; ?>" width="48" height="48" alt="ArrOw"></div>
        <p>Please wait...</p>
    </div>
</div>

<nav class="navbar custom-navbar navbar-expand-lg py-2">
    <div class="container-fluid px-0">
        <a href="javascript:void(0);" class="menu_toggle"><i class="fa fa-align-left"></i></a>
        <a href="index.html" class="navbar-brand"><img src="<?php echo base_url().'assets/frontend/images/illustrator/Startup_SVG.png'; ?>" style="width: 35px;"/>Sistem Pakar Bawang Merang</a>
        <div id="navbar_main">
            <ul class="navbar-nav mr-auto hidden-xs">
                <li class="nav-item page-header">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i></a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ul>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item hidden-xs">
                    <form class="form-inline main_search">
                        <input class="form-control form-control-sm mr-sm-2" type="search" placeholder="Search..." aria-label="Search">
                    </form>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link nav-link-icon" href="javascript:void(0);" id="navbar_1_dropdown_3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <h6 class="dropdown-header">User menu</h6>
                        <a class="dropdown-item" href="javascript:void(0);"><i class="fa fa-user text-primary"></i>My Profile</a>
                        <a class="dropdown-item" href="<?php echo base_url().'user/logout'; ?>"><i class="fa fa-sign-out text-primary"></i>Sign out</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="main_content" id="main-content">

    <div class="left_sidebar">
        <nav class="sidebar">
            <div class="user-info">
                <div class="image"><a href="javascript:void(0);"><img src="<?php echo base_url().'assets/frontend/images/illustrator/Startup_SVG.png'; ?>" style="width: 50px;" alt="User"></a></div>
                <div class="detail mt-3">
                    <h5 class="mb-0"><?php echo $this->session->userdata('nama');?></h5>
                    <small>User</small>
                </div>
                <div class="social">
                    <a href="javascript:void(0);" title="Facebook"><i class="fab fa-facebook"></i></a>
                    <a href="javascript:void(0);" title="Instagram"><i class="fab fa-instagram"></i></a>
                </div>
            </div>
            <ul id="main-menu" class="metismenu">
                <li class="g_heading">Main</li>
                <li class="active"><a href="<?php echo base_url().'user'; ?>"><i class="fas fa-home"></i><span>Dashboard</span></a></li>
                <li><a href="<?php echo base_url().'user/konsultasi'; ?>"><i class="fas fa-list"></i><span>Konsultasi</span></a></li>
                <li><a href="<?php echo base_url().'user/riwayat_konsultasi'; ?>"><i class="fas fa-chart-bar"></i><span>Riwayat Konsultasi</span></a></li>
            </ul>            
        </nav>
    </div>

    <div class="right_sidebar">
        <div class="setting_div">
            <a href="javascript:void(0);" class="rightbar_btn"><i class="fa fa-cog fa-spin"></i></a>
        </div>
        <hr>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane show active" id="Settings" role="tabpanel" aria-labelledby="Settings-tab">
                <div class="sidebar-scroll">
                    <div class="sidebar-widget px-3">
                        <h5>Theme Setting</h5>
                        <ul class="choose-skin list-unstyled">
                            <li data-theme="black"><div class="black"></div></li>
                            <li data-theme="azure"><div class="azure"></div></li>
                            <li data-theme="indigo" class="active"><div class="indigo"></div></li>
                            <li data-theme="purple"><div class="purple"></div></li>
                            <li data-theme="orange"><div class="orange"></div></li>
                            <li data-theme="green"><div class="green"></div></li>                                
                            <li data-theme="cyan"><div class="cyan"></div></li>
                            <li data-theme="blush"><div class="blush"></div></li>
                        </ul>
                        <ul class="setting-list list-unstyled mt-3">
                            <li>
                                <label class="custom-switch">
                                    <span class="custom-switch-description">Dark Sidebar</span>
                                    <label class="toggle-switch switch-sm mb-0">
                                        <input type="checkbox" class="switch-dark">
                                        <span class="toggle-switch-slider"></span>
                                    </label>
                                </label>
                            </li>
                            <li>
                                <label class="custom-switch">
                                    <span class="custom-switch-description">Mini Sidebar</span>
                                    <label class="toggle-switch switch-sm mb-0">
                                        <input type="checkbox" class="switch-sidebar">
                                        <span class="toggle-switch-slider"></span>
                                    </label>
                                </label>
                            </li>
                        </ul>
                        <hr>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
