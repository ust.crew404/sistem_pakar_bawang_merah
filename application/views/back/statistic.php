    <div class="page">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="javascript:void(0);">Riwayat User</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-align-justify"></i>
            </button>
        </nav>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-6 col-sm-12">
                    <div class="card widget_2 big_icon traffic">
                        <div class="body">
                          <div class="table-responsive">
                            <div id="DataTables_Table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                    <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="DataTables_Table_1" role="grid" aria-describedby="DataTables_Table_1_info">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="No: activate to sort column descending" style="width: 100px;">No</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="URL: activate to sort column ascending" style="width: 397px;">Nama</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="Short: activate to sort column ascending" style="width: 187px;">Gejala</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="Slug: activate to sort column ascending" style="width: 201px;">Penyakit</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="Description: activate to sort column ascending" style="width: 281px;">Solusi</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="Description: activate to sort column ascending" style="width: 281px;">Hasil Konsultasi</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="Description: activate to sort column ascending" style="width: 281px;">Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th rowspan="1" colspan="1">No</th>
                                                <th rowspan="1" colspan="1">Nama</th>
                                                <th rowspan="1" colspan="1">Gejala</th>
                                                <th rowspan="1" colspan="1">Penyakit</th>
                                                <th rowspan="1" colspan="1">Solusi</th>
                                                <th rowspan="1" colspan="1">Hasil Konsultasi</th>
                                                <th rowspan="1" colspan="1">Keterangan</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        $no=1;
                                        foreach($riwayat_user as $rk){
                                            ?>     
                                            <tr role="row" class="odd">
                                                <td class="sorting_1"><?php echo $no++?></td>
                                                <td><?php echo $rk->nama ?></td>
                                                <td align="justify"><?php echo $rk->gejala ?></td>
                                                <td><?php echo $rk->penyakit ?></td>
                                                <td align="justify"><?php echo $rk->solusi ?></td>
                                                <td><?php echo $rk->hasilkonsultasi ?></td>
                                                <td><?php echo $rk->keterangan ?></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>    
        </div>

