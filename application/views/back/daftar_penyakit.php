    <div class="page">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="javascript:void(0);">Daftar Penyakit</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-align-justify"></i>
            </button>
        </nav>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-6 col-sm-12">
                    <div class="card widget_2 big_icon traffic">
                        <div class="body">
                            <!-- Button trigger modal -->

                            <p><font color="red"><?php echo $this->session->flashdata('input'); ?></font> <font color="green"><?php echo $this->session->flashdata('sukses'); ?></font></p>
                            <div class="col-lg-2">
                                <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#modal_5">
                                Tambah Penyakit
                                </button>
                            </div>
                            <hr>
                                <!-- Modal -->
                                <div class="modal modal-fluid fade" id="modal_5" tabindex="-1" role="dialog" aria-labelledby="modal_5" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="modal_title_6">Tambah Penyakit</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="<?php echo base_url().'admin/add_penyakit'; ?>" method="post">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Kode Penyakit</span>
                                                    </div>
                                                    <input type="text" class="form-control" placeholder="Kode Penyakit" aria-label="Kode Gejala" aria-describedby="basic-addon1" name="kg" required="">
                                                </div>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Nama Penyakit</span>
                                                    </div>
                                                    <input type="text" class="form-control" placeholder="Nama Penyakit" aria-label="slug" name="ng" aria-describedby="basic-addon1" maxlength="10">
                                                </div>
                                                <div class="input-group mb-3">
                                                    <input type="submit" class="btn btn-block btn-primary active" value="Add">
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">close</button>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="table-responsive">
                            <div id="DataTables_Table_1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                    <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="DataTables_Table_1" role="grid" aria-describedby="DataTables_Table_1_info">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="No: activate to sort column descending" style="width: 100px;">No</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="URL: activate to sort column ascending" style="width: 397px;">Kode Penyakit</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="Short: activate to sort column ascending" style="width: 187px;">Nama Penyakit</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" aria-label="Short: activate to sort column ascending" style="width: 187px;"> </th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th rowspan="1" colspan="1">No</th>
                                                <th rowspan="1" colspan="1">Kode Penyakit</th>
                                                <th rowspan="1" colspan="1">Nama Penyakit</th>
                                                <th rowspan="1" colspan="1"> </th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                        $no=1;
                                        foreach($daftar_penyakit as $dg){
                                            ?>     
                                            <tr role="row" class="odd">
                                                <td class="sorting_1"><?php echo $no++?></td>
                                                <td><?php echo $dg->kode_penyakit ?></td>
                                                <td><?php echo $dg->nama_penyakit ?></td>
                                                <td><a href="<?php echo base_url().'admin/edit_penyakit/'.$dg->id; ?>"><i class="fas fa-edit"></i></a> <a href="<?php echo base_url().'admin/delete_penyakit/'.$dg->id; ?>"><i class="fas fa-trash-alt"></i></a></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>

